pragma solidity ^0.4.0;

contract Killable {
    address owner;
    
    modifier onlyowner() {
        if(owner != msg.sender) {
            throw;
        }
        _;
    }
    
    function Killable() {
        owner = msg.sender;
    }
    
    function kill() onlyowner {
        suicide(owner);
    }
}

contract Wallet is Killable{
    
    mapping(address => Permission) permittedAddresses;
    
    event addressAddedToSendersList(address thePersonWhoAdded, address thePersonWhoIsAllowed, uint maxAmountToSend);
    event addressRemovedFromSendersList(address addressRemoved);
    event fundsSent(address from, address to, uint amountInWei);
    
    struct Permission {
        bool isAllowed;
        uint maxTransferAmount;
    }
    
    function addAddressToSendersList(address permitted, uint maxTransferAmount) onlyowner {
        permittedAddresses[permitted] = Permission(true, maxTransferAmount);
        addressAddedToSendersList(msg.sender, permitted, maxTransferAmount);
    }
    
    function sendFunds(address receiver, uint amountInWei) {
        if(!permittedAddresses[msg.sender].isAllowed) {
            throw;
        }
        
        if(permittedAddresses[msg.sender].maxTransferAmount <= amountInWei) {
            throw;    
        }
        
        receiver.transfer(amountInWei);
        fundsSent(msg.sender, receiver, amountInWei);
    }
    
    function removeAddressFromSendersList(address theAddress) {
        delete permittedAddresses[theAddress];
        addressRemovedFromSendersList(theAddress);
    }
    
    function () payable {}
}